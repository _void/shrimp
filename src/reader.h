#ifndef READER_H_
#define READER_H_
#define BUFSIZE 256

char *readline(char *prompt);
void add_history(char *com);
void write_history(char *filename);

#endif
