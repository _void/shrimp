#ifndef PARSER_H_
#define PARSER_H_

#define DELIM " \n\t\r"

struct exec_ctx parse_string(char *str);
void clean_vector(struct exec_ctx ctx);

#endif
