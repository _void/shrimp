OBJ := main.o parser.o reader.o executor.o
CC = gcc
CFLAGS =-c -Wall -g
LDFLAGS =
PROG = shrimp

all: $(OBJ)
	$(CC) $(OBJ) $(LDFLAGS) -o $(PROG)

main.o: ./src/main.c ./src/parser.h ./src/errors.h
	$(CC) $(CFLAGS) ./src/main.c

parser.o: ./src/parser.c ./src/parser.h
	$(CC) $(CFLAGS) ./src/parser.c

reader.o: ./src/reader.c ./src/reader.h
	$(CC) $(CFLAGS) ./src/reader.c

executor.o: ./src/executor.c ./src/executor.h
	$(CC) $(CFLAGS) ./src/executor.c

clean:
	rm $(PROG) *.o
