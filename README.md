# SHRIMP
Simple shell written in C language
## Build
```shell
make
```
## Usage
```shell
./shrimp
```
# LICENSE
[MIT](./LICENSE)
