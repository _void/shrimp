#ifndef VAR_H_
#define VAR_H_

#define NAMEBUF 128
#define VALUEBUF 4096

typedef enum { INT, STRING } types;

typedef struct {
  char *name;
  types type;
  char *data;
  void *next;
} node;

int add(char *key, char *value, types type);
int del(char *key);
node *lookup(char *key);
node *get_head(void);
void clean_list(void);

#endif
