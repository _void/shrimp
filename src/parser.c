#include "parser.h"
#include "executor.h"
#include <stdlib.h>
#include <string.h>

int contains(char *str, char ch) {
  while (*str) {
    if (*str == ch)
      return 1;
    str++;
  }
  return 0;
}

struct exec_ctx parse_string(char *str) {
  struct exec_ctx ctx = {0};
  int len = 10;
  int pos = 0;
  ctx.args = malloc(sizeof(char *) * len);
  if (ctx.args == NULL)
    return ctx;
  char *token = strtok(str, DELIM);
  if (token == NULL)
    return ctx;
  ctx.args[pos++] = token;

  while (token != NULL) {
    token = strtok(NULL, DELIM);
    ctx.args[pos++] = token;
    // if not enough space - add space!
    if (pos == len - 1) {
      len *= 2;
      ctx.args = realloc(ctx.args, len);
    }
  }
  ctx.args[pos] = "\0";
  ctx.in = 0;
  ctx.out = 1;
  return ctx;
}

void clean_vector(struct exec_ctx ctx) { free(ctx.args); }
