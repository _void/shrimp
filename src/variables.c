#include "variables.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

node *head = NULL;

int add(char *key, char *value, types type) {
  node *new = malloc(sizeof(node));
  new->type = type;
  new->name = strdup(key);
  if (new->name == NULL)
    return -1;

  new->data = strdup(value);
  if (new->data == NULL)
    return -1;

  new->next = head;
  head = new;
  return 0;
}

node *lookup(char *key) {
  node *current = head;

  while (current != NULL) {
    if (strncmp(current->name, key, NAMEBUF) == 0) {
      return current;
    }
    current = current->next;
  }
  return NULL;
}

int del(char *key) {
  node *cur = head;
  node *prev = NULL;

  while (cur != NULL) {
    if (strncmp(cur->name, key, NAMEBUF) == 0) {
      prev->next = cur->next;
      free(cur->name);
      free(cur->data);
      free(cur);
    }
    prev = cur;
    cur = cur->next;
  }

  return 0;
}

node *get_head(void) { return head; }

void clean_list(void) {
  node *cur = head;
  while (cur != NULL) {
    head = head->next;
    free(cur->data);
    free(cur->name);
    free(cur);
    cur = head;
  }
}
