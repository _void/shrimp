#include "errors.h"
#include "executor.h"
#include "parser.h"
#include "reader.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#define PATHBUF 128

// Default values
static char *home = NULL;
static char *prev = NULL;
static char *history_file = NULL;

// Builtin
int cd(char *arg) {

  if (arg == NULL || arg[0] == '~') {
    getcwd(prev, PATHBUF);

    if (arg != NULL && strlen(arg) > 2) {
      char *temp = malloc(PATHBUF);
      if (temp == NULL)
        return -1;

      snprintf(temp, PATHBUF, "%s%s", home, arg + 1);

      if (chdir(temp) == -1)
        return -2;

      free(temp);
    } else {
      if (chdir(home) == -1)
        return -2;
    }
  } else {
    if (chdir(arg) == -1)
      return -2;
  }
  return 0;
}

int history() {
  FILE *h = fopen(history_file, "r");
  if (h == NULL)
    return -1;
  char *buffer = malloc(BUFSIZE);
  if (buffer == NULL)
    return -2;

  while (!feof(h)) {
    fgets(buffer, BUFSIZE - 1, h);
    printf("%s", buffer);
  }
  fclose(h);
  free(buffer);
  return 0;
}

// Main loop
int shrimp_loop(void) {
  int return_code = 0;

  while (1) {
    char *input = readline("shrimp> ");
    add_history(input);

    struct exec_ctx ctx = parse_string(input);
    if (ctx.args == NULL) {
      return_code = EPARSE;
      break;
    }

    // Command execution
    if (strncmp(ctx.args[0], "exit", 5) == 0) {
      free(input);
      clean_vector(ctx);
      printf("[Goodbye]\n");
      break;

    } else if (strncmp(ctx.args[0], "cd", 3) == 0) {
      int status = cd(ctx.args[1]);
      if (status == -2)
        printf("[%s Doesn't exists]\n", ctx.args[1]);

    } else if (strncmp(ctx.args[0], "pwd", 4) == 0) {
      char *buf = malloc(PATHBUF);
      if (buf == NULL) {
        return_code = EMALLOC;
        break;
      }

      getcwd(buf, PATHBUF);
      printf("%s\n", buf);
      free(buf);

    } else if (strncmp(ctx.args[0], "history", 8) == 0) {
      history();
    } else {
      execute(ctx);
    }

    free(input);
    clean_vector(ctx);
  }

  return return_code;
}

int main(int argc, char *argv[]) { // in future will add arguments parsing
  printf("\033[0;0m");             // clear colors
  home = getenv("HOME");
  prev = malloc(PATHBUF);
  history_file = malloc(PATHBUF);
  if (prev == NULL || history_file == NULL)
    return 1;
  memset(prev, 0, PATHBUF);

  snprintf(history_file, PATHBUF, "%s/%s", home, ".shrimp_history");

  int interactive = 1;
  int ecode; // if something went wrong main loop will return error code

  if (interactive)
    ecode = shrimp_loop();

  write_history(history_file);
  free(history_file);
  free(prev);
  return ecode;
}
