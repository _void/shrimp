#include "executor.h"
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

int execute(struct exec_ctx ctx) {
  int status;
  pid_t child = fork();
  switch (child) {
  case 0:
    if (ctx.in != 0 && ctx.out != 1) {
      dup2(0, ctx.in);
      dup2(1, ctx.out);
    }
    if (execvp(ctx.args[0], ctx.args) < 0) {
      fprintf(stderr, "[Something went wrong]\n");
      _exit(1);
    }
    break;
  case -1:
    fprintf(stderr, "[Unexpected error]\n");
    return 1;
  default:
    waitpid(child, &status, 0);
    if (status != 0)
      fprintf(stderr, "[Unknown command]\n");
    break;
  }
  return 0;
}
