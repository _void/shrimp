#include "reader.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static char **history = NULL;
static int pos = 0;
static int size = 16;

int init_history(void) {
  history = malloc(size * sizeof(char *));
  if (history == NULL)
    return 1;
  return 0;
}

char *readline(char *prompt) {
  printf("%s", prompt);
  int size = 4;
  char *buf = malloc(size);
  int pos = 0;

  int ch;
  while ((ch = getchar()) != '\n') {
    if (pos == size - 1) {
      size *= 2;
      buf = realloc(buf, size);
    }
    buf[pos++] = ch;
  }
  buf[pos] = '\0';
  return buf;
}

void add_history(char *com) {
  if (history == NULL)
    if (init_history())
      exit(1);
  if (pos == size) {
    size *= 2;
    history = realloc(history, size * sizeof(char *));
  }
  history[pos++] = strdup(com);
}

void write_history(char *filename) {
  int f = open(filename, O_WRONLY | O_CREAT, 0644);
  if (f == -1)
    exit(1);

  for (int i = 0; i < pos; i++) {
    write(f, history[i], strlen(history[i]));
    write(f, "\n", 1);
  }
  close(f);
}
