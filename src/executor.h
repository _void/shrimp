#ifndef EXEC_H_
#define EXEC_H_

struct exec_ctx {
  char **args;
  int in;
  int out;
};

int execute(struct exec_ctx ctx);

#endif
